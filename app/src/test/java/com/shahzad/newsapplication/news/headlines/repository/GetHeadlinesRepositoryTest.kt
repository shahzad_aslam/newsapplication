package com.shahzad.newsapplication.news.headlines.repository

import com.nhaarman.mockitokotlin2.whenever
import com.shahzad.newsapplication.base.util.ICoroutineContextProvider
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import com.shahzad.newsapplication.news.headlines.network.Source
import com.shahzad.newsapplication.news.network.api.NewsServiceApi
import com.shahzad.newsapplication.util.TestCoroutineContextProvider
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks
import retrofit2.Response

class GetHeadlinesRepositoryTest {
    @Mock
    private lateinit var service: NewsServiceApi

    private lateinit var coroutineContextProvider: ICoroutineContextProvider
    private lateinit var repository: IGetHeadlinesRepository

    @Before
    fun setUp() {
        initMocks(this)
        coroutineContextProvider = TestCoroutineContextProvider()
        repository = GetHeadlinesRepository(service, coroutineContextProvider)
    }

    @Test
    fun `getTopHeadlines return HeadlinesResponse`() = runBlocking {
        val model = getHeadlinesResponse()
        whenever(service.getTopHeadlines(anyString(), anyString(), anyInt(), anyInt())).thenReturn(
            Response.success(model)
        )

        val details = repository.getHeadlines(5, 1)

        assertTrue(details.isResult)
        assertEquals(model, details.result())
    }

    @Test
    fun `getTopHeadlines return Error`() = runBlocking {
        val errorBody = "".toResponseBody(null)
        whenever(service.getTopHeadlines(anyString(), anyString(), anyInt(), anyInt())).thenReturn(
            Response.error(400, errorBody)
        )

        val details = repository.getHeadlines(5, 1)
        assertTrue(details.isError)
    }

    private fun getHeadlinesResponse(): HeadlinesResponse {
        return HeadlinesResponse(
            status = "ok",
            totalResults = 1,
            articles = listOf(
                Article(
                    title = "title",
                    description = "deription",
                    urlToImage = "image_url",
                    url = "some_url",
                    source = Source(
                        id = "source_id",
                        name = "source_name"
                    ),
                    author = "Author"
                )
            )
        )
    }
}