package com.shahzad.newsapplication.news.headlines.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.shahzad.newsapplication.base.exception.NewsAppException
import com.shahzad.newsapplication.base.functional.Either
import com.shahzad.newsapplication.news.headlines.interactor.IGetHeadlinesInteractor
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import com.shahzad.newsapplication.news.headlines.network.Source
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState.HeadlineError.ServerError
import com.shahzad.newsapplication.util.TestCoroutineRule
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class HeadlineViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var interactor: IGetHeadlinesInteractor
    @Mock
    private lateinit var observer: Observer<HeadlineViewState>

    private lateinit var viewModel: HeadlineViewModel
    private lateinit var model: HeadlinesResponse
    private lateinit var config: PagedList.Config

    @Before
    fun setUp() {
        initMocks(this)
        config = getPageConfig()
        model = getHeadlinesResponse()
    }

    @Test
    fun `getHeadlines update livedata`() = runBlockingTest {
        whenever(runBlocking { interactor.getHeadlines(anyInt(), anyInt()) }).thenReturn(Either.Result(model))

        viewModel = HeadlineViewModel(interactor, config)
        viewModel.headlineViewState.observeForever(observer)

        verify(observer).onChanged(any())
    }

    @Test
    fun `getHeadlines return error`() = runBlockingTest {
        val error = NewsAppException.ServerError
        whenever(runBlocking { interactor.getHeadlines(anyInt(), anyInt()) }).thenReturn(Either.Error(error))

        viewModel = HeadlineViewModel(interactor, config)
        viewModel.headlineViewState.observeForever(observer)

        verify(observer).onChanged(ServerError)
    }

    private fun getHeadlinesResponse(): HeadlinesResponse {
        return HeadlinesResponse(
            status = "ok",
            totalResults = 1,
            articles = listOf(
                Article(
                    title = "title",
                    description = "deription",
                    urlToImage = "image_url",
                    url = "some_url",
                    source = Source(
                        id = "source_id",
                        name = "source_name"
                    ),
                    author = "Author"
                )
            )
        )
    }

    private fun getPageConfig() =
        PagedList.Config.Builder()
            .setInitialLoadSizeHint(10)
            .setPageSize(5)
            .setPrefetchDistance(2)
            .build()
}