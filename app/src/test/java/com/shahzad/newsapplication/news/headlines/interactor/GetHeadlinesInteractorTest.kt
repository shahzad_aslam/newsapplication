package com.shahzad.newsapplication.news.headlines.interactor

import com.nhaarman.mockitokotlin2.whenever
import com.shahzad.newsapplication.base.exception.NewsAppException
import com.shahzad.newsapplication.base.functional.Either
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import com.shahzad.newsapplication.news.headlines.network.Source
import com.shahzad.newsapplication.news.headlines.repository.IGetHeadlinesRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

class GetHeadlinesInteractorTest {
    @Mock
    private lateinit var repository: IGetHeadlinesRepository

    private lateinit var interactor: IGetHeadlinesInteractor

    @Before
    fun setUp() {
        initMocks(this)
        interactor = GetHeadlinesInteractor(repository)
    }

    @Test
    fun `returns the data if repo have data`() = runBlocking {
        val model = getHeadlinesResponse()
        whenever(repository.getHeadlines(anyInt(), anyInt())).thenReturn(Either.Result(model))

        val receivedData = interactor.getHeadlines(5, 1)

        assertEquals(model, receivedData.result())
    }

    @Test
    fun `returns error if repo has error`() = runBlocking {
        val error = NewsAppException.ServerError
        whenever(repository.getHeadlines(anyInt(), anyInt())).thenReturn(Either.Error(error))

        val receivedData = interactor.getHeadlines(5, 1)

        assertEquals(error, receivedData.error())
    }

    private fun getHeadlinesResponse(): HeadlinesResponse {
        return HeadlinesResponse(
            status = "ok",
            totalResults = 1,
            articles = listOf(
                Article(
                    title = "title",
                    description = "deription",
                    urlToImage = "image_url",
                    url = "some_url",
                    source = Source(
                        id = "source_id",
                        name = "source_name"
                    ),
                    author = "Author"
                )
            )
        )
    }
}