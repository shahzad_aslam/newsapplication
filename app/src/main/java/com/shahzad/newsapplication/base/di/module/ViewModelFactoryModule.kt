package com.shahzad.newsapplication.base.di.module

import androidx.lifecycle.ViewModelProvider
import com.shahzad.newsapplication.base.viewmodel.BaseViewModelFactory
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ViewModelFactoryModule {
    @[Singleton Binds]
    fun bindViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory
}