package com.shahzad.newsapplication.base.di.module

import com.shahzad.newsapplication.base.di.scope.ActivityScope
import com.shahzad.newsapplication.news.di.module.ServiceModule
import com.shahzad.newsapplication.news.view.NewsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {
    @[ActivityScope ContributesAndroidInjector(modules = [ServiceModule::class, FragmentBindingModule::class])]
    fun injectNewsActivity(): NewsActivity
}