package com.shahzad.newsapplication.base.view

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {
    @LayoutRes
    open val layoutResource = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
    }

    fun replaceFragmentAndAddToBackStack(@IdRes id: Int, fragment: BaseFragment, tag: String) {
        supportFragmentManager.beginTransaction().replace(id, fragment).addToBackStack(tag).commit()
    }

    fun replaceFragment(@IdRes id: Int, fragment: BaseFragment, tag: String) {
        supportFragmentManager.beginTransaction().replace(id, fragment, tag).commit()
    }
}