package com.shahzad.newsapplication.base.util

import androidx.appcompat.widget.AppCompatImageView
import com.shahzad.newsapplication.R
import com.squareup.picasso.Picasso

fun AppCompatImageView.loadImage(url: String?) {
    if (!url.isNullOrBlank()) {
        Picasso.get()
            .load(url)
            .placeholder(R.drawable.loading_indicator)
            .error(android.R.drawable.ic_dialog_alert)
            .into(this)
    }
}