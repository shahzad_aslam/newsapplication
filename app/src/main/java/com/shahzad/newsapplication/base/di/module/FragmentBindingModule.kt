package com.shahzad.newsapplication.base.di.module

import com.shahzad.newsapplication.base.di.scope.FragmentScope
import com.shahzad.newsapplication.news.headlinedetail.view.NewsDetailFragment
import com.shahzad.newsapplication.news.headlines.di.module.HeadlineModule
import com.shahzad.newsapplication.news.headlines.view.HeadlineFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBindingModule {
    @[FragmentScope ContributesAndroidInjector(modules = [HeadlineModule::class])]
    fun injectHeadlineFragment(): HeadlineFragment

    @[FragmentScope ContributesAndroidInjector(modules = [])]
    fun injectNewsDetailFragment(): NewsDetailFragment
}