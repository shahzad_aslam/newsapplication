package com.shahzad.newsapplication.base.di.module

import com.shahzad.newsapplication.base.util.CoroutineContextProvider
import com.shahzad.newsapplication.base.util.ICoroutineContextProvider
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface CoroutineModule {
    @[Singleton Binds]
    fun bindCoroutineContextProvider(contextProvider: CoroutineContextProvider): ICoroutineContextProvider
}