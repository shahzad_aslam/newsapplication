package com.shahzad.newsapplication.base.exception

sealed class NewsAppException {
    object NetworkConnection : NewsAppException()
    object ServerError : NewsAppException()
}