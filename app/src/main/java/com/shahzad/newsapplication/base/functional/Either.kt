/**
 * Inspiration from clean code architecture by Fernando Cejas Open Source Project
 */

package com.shahzad.newsapplication.base.functional

sealed class Either<out E, out R> {
    data class Error<out E>(val data: E) : Either<E, Nothing>()
    data class Result<out R>(val data: R) : Either<Nothing, R>()

    val isResult get() = this is Result<R>
    val isError get() = this is Error<E>

    fun error(): E = (this as Error<E>).data

    fun result(): R = (this as Result<R>).data

    fun either(fnL: (E) -> Any, fnR: (R) -> Any): Any =
        when (this) {
            is Error -> fnL(data)
            is Result -> fnR(data)
        }
}