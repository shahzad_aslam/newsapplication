package com.shahzad.newsapplication.base.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ContextModule {
    @[Singleton Binds]
    fun bindContext(app: Application) : Context
}