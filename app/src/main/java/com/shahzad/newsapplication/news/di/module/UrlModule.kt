package com.shahzad.newsapplication.news.di.module

import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
object UrlModule {
    const val BASE_URL_QUALIFIER = "SERVICE_URL"

    @[Provides Named(BASE_URL_QUALIFIER)]
    fun provideNewsServiceUrl() = "https://newsapi.org/"
}