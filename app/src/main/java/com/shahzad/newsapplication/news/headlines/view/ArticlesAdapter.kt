package com.shahzad.newsapplication.news.headlines.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.shahzad.newsapplication.R
import com.shahzad.newsapplication.base.util.loadImage
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.view.ArticlesAdapter.ArticleViewHolder
import kotlinx.android.synthetic.main.headline_item.view.*
import javax.inject.Inject

class ArticlesAdapter @Inject constructor() : PagedListAdapter<Article, ArticleViewHolder>(DiffUtilCallback()) {
    internal var clickListener: (Article) -> Unit = { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.headline_item, parent, false)
        return ArticleViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        getItem(position)?.let { holder.setItem(it) }
    }

    inner class ArticleViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun setItem(item: Article) {
            view.image.loadImage(item.urlToImage)
            view.title.text = item.title ?: ""
            view.content.text = item.description ?: ""
            view.source.text = view.context.getString(R.string.news_source, item.source.name)
            view.setOnClickListener { clickListener(item) }
        }
    }
}

class DiffUtilCallback : DiffUtil.ItemCallback<Article>() {
    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem.title == newItem.title
    }

    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
        return oldItem == newItem
    }
}