package com.shahzad.newsapplication.news.headlines.repository

import com.shahzad.newsapplication.base.exception.NewsAppException
import com.shahzad.newsapplication.base.functional.Either
import com.shahzad.newsapplication.base.util.ICoroutineContextProvider
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import com.shahzad.newsapplication.news.network.api.NewsServiceApi
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface IGetHeadlinesRepository {
    suspend fun getHeadlines(pageSize: Int, pageNumber: Int): Either<NewsAppException, HeadlinesResponse>
}

class GetHeadlinesRepository @Inject constructor(
    private val service: NewsServiceApi,
    private val coroutineContextProvider: ICoroutineContextProvider
) : IGetHeadlinesRepository {
    override suspend fun getHeadlines(pageSize: Int, pageNumber: Int): Either<NewsAppException, HeadlinesResponse> {
        return withContext(coroutineContextProvider.io) {
            try {
                val response = service.getTopHeadlines(pageSize = pageSize, page = pageNumber)
                if (response.isSuccessful) {
                    Either.Result(response.body()!!)
                } else {
                    Either.Error(NewsAppException.ServerError)
                }
            } catch (exception: Exception) {
                Either.Error(NewsAppException.NetworkConnection)
            }
        }
    }
}