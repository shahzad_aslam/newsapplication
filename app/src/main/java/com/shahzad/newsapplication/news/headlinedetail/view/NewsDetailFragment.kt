package com.shahzad.newsapplication.news.headlinedetail.view

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.annotation.LayoutRes
import com.shahzad.newsapplication.R
import com.shahzad.newsapplication.base.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_news_detail.view.*

class NewsDetailFragment : BaseFragment() {
    private lateinit var progressBar : ProgressBar
    private lateinit var webView : WebView

    companion object {
        private const val URL = "URL"
        fun newInstance(url: String): NewsDetailFragment {
            return NewsDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(URL, url)
                }
            }
        }
    }

    @get:LayoutRes
    override val layoutResource: Int = R.layout.fragment_news_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView = view.web_view
        progressBar = view.progress_bar
        setUpWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setUpWebView() {
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar.visibility = View.INVISIBLE
                super.onPageFinished(view, url)
            }
        }

        webView.loadUrl(arguments!!.getString(URL))
    }
}