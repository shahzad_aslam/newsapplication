package com.shahzad.newsapplication.news.headlines.di.module

import androidx.lifecycle.ViewModel
import com.shahzad.newsapplication.base.di.scope.FragmentScope
import com.shahzad.newsapplication.base.di.util.ViewModelKey
import com.shahzad.newsapplication.news.headlines.interactor.GetHeadlinesInteractor
import com.shahzad.newsapplication.news.headlines.interactor.IGetHeadlinesInteractor
import com.shahzad.newsapplication.news.headlines.repository.GetHeadlinesRepository
import com.shahzad.newsapplication.news.headlines.repository.IGetHeadlinesRepository
import com.shahzad.newsapplication.news.headlines.viewmodel.HeadlineViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [PagingModule::class])
interface HeadlineModule {
    @Binds
    fun bindGetHeadlineInteractor(interactor: GetHeadlinesInteractor): IGetHeadlinesInteractor

    @Binds
    fun bindGetHeadlineRepository(repository: GetHeadlinesRepository): IGetHeadlinesRepository

    @FragmentScope
    @Binds
    @IntoMap
    @ViewModelKey(HeadlineViewModel::class)
    fun bindHeadlineViewModel(headlineViewModel: HeadlineViewModel): ViewModel
}