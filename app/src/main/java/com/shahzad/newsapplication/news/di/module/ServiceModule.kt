package com.shahzad.newsapplication.news.di.module

import com.shahzad.newsapplication.base.di.scope.ActivityScope
import com.shahzad.newsapplication.news.di.module.UrlModule.BASE_URL_QUALIFIER
import com.shahzad.newsapplication.news.network.api.NewsServiceApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module(includes = [UrlModule::class])
object ServiceModule {
    @[ActivityScope Provides]
    fun provideNewsServiceApi(
        retrofitBuilder: Retrofit.Builder,
        @Named(BASE_URL_QUALIFIER) url: String
    ): NewsServiceApi {
        return retrofitBuilder.baseUrl(url).build()
            .create(NewsServiceApi::class.java)
    }
}
