package com.shahzad.newsapplication.news.view

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.shahzad.newsapplication.R
import com.shahzad.newsapplication.base.view.BaseActivity
import com.shahzad.newsapplication.news.headlinedetail.view.NewsDetailFragment
import com.shahzad.newsapplication.news.headlines.view.HeadlineFragment

class NewsActivity : BaseActivity() {

    companion object {
        private const val HEADLINE_FRAGMENT_TAG = "HEADLINE_FRAGMENT"
        private const val NEWS_DETAIL_FRAGMENT_TAG = "NEWS_DETAIL_FRAGMENT"
    }

    @get:LayoutRes
    override val layoutResource: Int = R.layout.activity_news

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            replaceFragment(R.id.container, HeadlineFragment.newInstance(), HEADLINE_FRAGMENT_TAG)
        }
    }

    fun showNewsDetailsDetails(url: String) {
        replaceFragmentAndAddToBackStack(R.id.container, NewsDetailFragment.newInstance(url), NEWS_DETAIL_FRAGMENT_TAG)
    }

}
