package com.shahzad.newsapplication.news.headlines.view

import androidx.paging.PagedList
import com.shahzad.newsapplication.news.headlines.network.Article

sealed class HeadlineViewState {
    data class ArticlesData(val data: PagedList<Article>) : HeadlineViewState()

    sealed class HeadlineError : HeadlineViewState() {
        object NoInternetConnection : HeadlineError()
        object ServerError : HeadlineError()
    }
}