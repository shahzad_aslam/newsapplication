package com.shahzad.newsapplication.news.headlines.view

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shahzad.newsapplication.R
import com.shahzad.newsapplication.base.view.BaseFragment
import com.shahzad.newsapplication.base.viewmodel.BaseViewModelFactory
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState.*
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState.HeadlineError.NoInternetConnection
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState.HeadlineError.ServerError
import com.shahzad.newsapplication.news.headlines.viewmodel.HeadlineViewModel
import com.shahzad.newsapplication.news.network.api.NewsServiceApi
import com.shahzad.newsapplication.news.view.NewsActivity
import kotlinx.android.synthetic.main.fragment_headline.*
import kotlinx.android.synthetic.main.fragment_headline.view.*
import javax.inject.Inject

class HeadlineFragment: BaseFragment() {
    @Inject
    lateinit var factory: BaseViewModelFactory
    @Inject
    lateinit var articlesAdapter: ArticlesAdapter


    private lateinit var viewModel: HeadlineViewModel

    companion object {
        fun newInstance(): HeadlineFragment {
            return HeadlineFragment()
        }
    }

    @get:LayoutRes
    override val layoutResource: Int = R.layout.fragment_headline

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(HeadlineViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.progress_bar.visibility = View.VISIBLE
        setUpHeadlinesList(view.recycler_view)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.headlineViewState.observe(viewLifecycleOwner, Observer {
            it?.let {
                when (it) {
                    is ArticlesData -> {
                        progress_bar.visibility = View.INVISIBLE
                        articlesAdapter.submitList(it.data)
                    }
                    is HeadlineError -> {
                        handleError(it)
                    }
                }
            }
        })
    }

    private fun handleError(error: HeadlineError) {
        when(error) {
            NoInternetConnection -> notifyWithAction(R.string.network_error, R.string.action_refresh) {}
            ServerError -> notifyWithAction(R.string.server_error, R.string.action_refresh) {}
        }
    }

    private fun setUpHeadlinesList(recyclerView: RecyclerView) {
        with(recyclerView) {
            val columns = resources.getInteger(R.integer.num_columns)
            val fullWidthColumnNumber = resources.getInteger(R.integer.full_width_column_number)
            layoutManager = GridLayoutManager(activity, columns).apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if (position % fullWidthColumnNumber == 0) columns else 1
                    }
                }
            }
            itemAnimator = DefaultItemAnimator()
            adapter = articlesAdapter
        }
        articlesAdapter.clickListener = { openArticleDetails(it) }
    }

    private fun openArticleDetails(article: Article) {
        (activity as NewsActivity).showNewsDetailsDetails(article.url)
    }

}