package com.shahzad.newsapplication.news.headlines.interactor

import com.shahzad.newsapplication.base.exception.NewsAppException
import com.shahzad.newsapplication.base.functional.Either
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import com.shahzad.newsapplication.news.headlines.repository.IGetHeadlinesRepository
import javax.inject.Inject

interface IGetHeadlinesInteractor {
    suspend fun getHeadlines(pageSize: Int, pageNumber: Int) : Either<NewsAppException, HeadlinesResponse>
}

class GetHeadlinesInteractor @Inject constructor(private val repository: IGetHeadlinesRepository): IGetHeadlinesInteractor {
    override suspend fun getHeadlines(pageSize: Int, pageNumber: Int): Either<NewsAppException, HeadlinesResponse> {
        return repository.getHeadlines(pageSize, pageNumber)
    }
}