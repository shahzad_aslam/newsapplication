package com.shahzad.newsapplication.news.headlines.di.module

import androidx.paging.PagedList
import com.shahzad.newsapplication.base.di.scope.FragmentScope
import dagger.Module
import dagger.Provides

@Module
object PagingModule {
    private const val PAGE_SIZE = 7
    private const val INITIAL_LOAD_SIZE = PAGE_SIZE * 3
    private const val PRE_FETCH_DISTANCE = 2

    @[FragmentScope Provides]
    fun providePagingConfig(): PagedList.Config =
        PagedList.Config.Builder()
            .setInitialLoadSizeHint(INITIAL_LOAD_SIZE)
            .setPageSize(PAGE_SIZE)
            .setPrefetchDistance(PRE_FETCH_DISTANCE)
            .setEnablePlaceholders(true)
            .build()
}