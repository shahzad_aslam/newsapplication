package com.shahzad.newsapplication.news.headlines.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.shahzad.newsapplication.base.exception.NewsAppException
import com.shahzad.newsapplication.news.headlines.interactor.IGetHeadlinesInteractor
import com.shahzad.newsapplication.news.headlines.network.Article
import com.shahzad.newsapplication.news.headlines.view.HeadlineViewState
import kotlinx.coroutines.launch
import javax.inject.Inject

class HeadlineViewModel @Inject constructor(
    private val interactor: IGetHeadlinesInteractor,
    private val config: PagedList.Config
): ViewModel() {

    companion object {
        private const val INITIAL_PAGE_KEY = 1
    }

    private val _headlineViewState = MediatorLiveData<HeadlineViewState>()
    val headlineViewState: LiveData<HeadlineViewState>
        get() = _headlineViewState

    init {
        _headlineViewState.addSource(initializePagedLiveData()) {
            _headlineViewState.value = HeadlineViewState.ArticlesData(it)
        }
    }

    private fun initializePagedLiveData(): LiveData<PagedList<Article>> {
        return LivePagedListBuilder<Int, Article>(object :
            DataSource.Factory<Int, Article>() {
            override fun create(): DataSource<Int, Article> {
                return ArticlesDataSource()
            }
        }, config).build()
    }

    private inner class ArticlesDataSource : PageKeyedDataSource<Int, Article>() {
        private fun handleException(exception: NewsAppException) {
            when (exception) {
                NewsAppException.NetworkConnection -> _headlineViewState.value = HeadlineViewState.HeadlineError.NoInternetConnection
                NewsAppException.ServerError -> _headlineViewState.value = HeadlineViewState.HeadlineError.ServerError
            }
        }

        override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Article>) {
            getHeadlines(params.requestedLoadSize, INITIAL_PAGE_KEY) { callback.onResult(it, null, 4) }
        }

        override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
            getHeadlines(params.requestedLoadSize, params.key) { callback.onResult(it, params.key + 1) }
        }

        override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
            getHeadlines(params.requestedLoadSize, params.key) { callback.onResult(it, params.key - 1) }
        }

        private fun getHeadlines(loadSize: Int, key: Int, callback: (List<Article>) -> Unit) {
            viewModelScope.launch {
                interactor.getHeadlines(loadSize, key).either(
                    { handleException(it) },
                    { callback(it.articles)}
                )
            }
        }
    }
}