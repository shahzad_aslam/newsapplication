package com.shahzad.newsapplication.news.network.api

import com.shahzad.newsapplication.BuildConfig
import com.shahzad.newsapplication.news.headlines.network.HeadlinesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsServiceApi {
    @GET("/v2/top-headlines")
    suspend fun getTopHeadlines(
        @Query("apiKey") apiKey: String = BuildConfig.news_api_key,
        @Query("language") language: String = "en",
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int
    ): Response<HeadlinesResponse>
}