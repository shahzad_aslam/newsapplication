**Information about NewsApplication** 

Built to showcase the Model-View-ViewModel architectural pattern.

Building and running the project You are free to clone this repo. This project requires:

Android Studio 3.5.3 Android Gradle Plugin 3.5.3 Kotlin 1.3.61

This app (along with the unit tests) are written 100% in Kotlin

- You need to add your NewsApiKey in gradle.properties file like
    - *NewsDebugApiKey=`<Your-DEBUG_API-KEY>`*
    - *NewsReleaseApiKey=`<Your-RELEASE-API-KEY>`*

This project makes use of the service https://newsapi.org/

Libraries Used
- Material Design Lib (For using material components in the application e.g. Snackbar and Recyclerview)
- AppCompat (For compatibility with older Android versions.)
- Dagger (For dependency injection, as it helps in really maintaining the clear separation between construction of object and how we use them in the code. Also it is easily extendable.)
- Retrofit (For network communication) - Moshi (For faster json parsing)
- Moshi (For faster json parsing)
- Architecture Components (For live data and viewmodels)
- PagingLibrary for using the pagination feature
- Coroutines (For thread handling and context switching)
- JUnit (for unit tests) - Mockito (For mocking the classes during testing)
- Coroutine-Test (Helper library for testing the coroutines as it helps in mocking the main thread.)